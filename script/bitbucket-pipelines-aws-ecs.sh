#!/usr/bin/env bash

# Define aws region for aws cli
export AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:-"eu-central-1"}

# Define local variables for deployment
export IMAGE_NAME="${DOCKER_HUB_USER}/${BITBUCKET_REPO_SLUG}:${BITBUCKET_BUILD_NUMBER}"

export ECS_CLUSTER_NAME="${BITBUCKET_REPO_OWNER}-${BITBUCKET_REPO_SLUG}-cluster"
export ECS_SERVICE_NAME="${BITBUCKET_REPO_OWNER}-${BITBUCKET_REPO_SLUG}-service"
export ECS_TASK_NAME="${BITBUCKET_REPO_OWNER}-${BITBUCKET_REPO_SLUG}-task"
export ECS_CONTAINER_NAME="${BITBUCKET_REPO_OWNER}-${BITBUCKET_REPO_SLUG}-container"
export ECS_LOG_GROUP="${BITBUCKET_REPO_OWNER}-${BITBUCKET_REPO_SLUG}-log-group"
export ECS_LOG_PREFIX="awslogs-${BITBUCKET_REPO_OWNER}-${BITBUCKET_REPO_SLUG}-task"

# Register ecs task definition
export TASK_VERSION=$(aws ecs register-task-definition --family "${ECS_TASK_NAME}" --cpu 1024 --memory 2048 --network-mode "awsvpc" --requires-compatibilities "FARGATE" --execution-role-arn ${ECS_SERVICE_ROLE_ARN} --task-role-arn ${ECS_TASK_ROLE_ARN} --container-definitions '[{"name":"'"${ECS_CONTAINER_NAME}"'","image":"'"${IMAGE_NAME}"'","portMappings":[{"containerPort":8080,"protocol":"http"}],"logConfiguration":{"logDriver":"awslogs","options":{"awslogs-group":"'"${ECS_LOG_GROUP}"'","awslogs-region":"'"${AWS_DEFAULT_REGION}"'","awslogs-stream-prefix":"'"${ECS_LOG_PREFIX}"'"}},"essential":true}]' | jq --raw-output '.taskDefinition.revision')

# Print ecs task version
echo "Registered ecs task definition [${TASK_VERSION}]"

# Update ecs service task definition task definition
aws ecs update-service --cluster "${ECS_CLUSTER_NAME}" --service "${ECS_SERVICE_NAME}" --task-definition "${ECS_TASK_NAME}:${TASK_VERSION}"
