/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.interstellar.construction.configuration;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class to handle test cases for interstellar configuration operations:
 *
 * <ul>
 *  <li> get interstellar s3 client
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@ExtendWith(SpringExtension.class)
class InterstellarConfigurationTest {

    /**
     * Interstellar configuration bean used for testing purposes.
     */
    private InterstellarConfiguration interstellarConfiguration;

    /**
     * Init the configuration class before running test cases.
     */
    @BeforeEach
    void initBean() {
        interstellarConfiguration = new InterstellarConfiguration();
    }


    @Test
    void given_configuration_validate_interstellar_s3_client() {

        final AmazonS3 amazonS3Client = interstellarConfiguration.interstellarS3Client();
        assertNotNull(amazonS3Client);
    }

    @Test
    void given_configuration_validate_interstellar_s3_client_credentials() {

        final AmazonS3 amazonS3Client = interstellarConfiguration.interstellarS3Client();

        final List<Bucket> buckets = amazonS3Client.listBuckets();
        assertTrue(0 < buckets.size());
    }

}
