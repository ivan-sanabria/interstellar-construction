/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.interstellar.construction.service;

import com.interstellar.construction.dto.GridReference;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

/**
 * Class to handle test cases for process image service operations:
 *
 * <ul>
 *  <li> process grid reference
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@ExtendWith(SpringExtension.class)
class ProcessImageServiceTest {

    /**
     * Mock of Geo Image Data Service.
     */
    @MockBean
    private GeoImageDataService geoImageDataService;

    /**
     * Mock of Converter Image Service.
     */
    @MockBean
    private ConverterImageService converterImageService;

    /**
     * Mock of Process Image Service.
     */
    private ProcessImageService processImageService;

    /**
     * Init the necessary mocks before running test cases.
     */
    @BeforeEach
    void initBean() {
        processImageService = new ProcessImageService(geoImageDataService, converterImageService);
    }


    @Test
    void given_null_grid_reference_return_empty_byte_response() {

        final byte[] response = processImageService.processGridReference(null);
        assertEquals(0, response.length);
    }

    @Test
    void given_empty_grid_reference_return_empty_byte_response() {

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();
        final byte[] mockImage = new byte[0];

        given(converterImageService.generateJpeg(foundImageBands, "T0nullnull_UNKNOWN_null.null"))
                .willReturn(mockImage);

        final byte[] response = processImageService.processGridReference(new GridReference());
        assertEquals(0, response.length);
    }

    @Test
    void given_invalid_grid_reference_return_empty_byte_response() {

        final GridReference gridReference = new GridReference();

        gridReference.setUtmZone(100);
        gridReference.setLatitudeBand("not supported band");
        gridReference.setGridSquare("not supported square");
        gridReference.setDate(LocalDate.now());
        gridReference.setChannelMap("not supported channel");

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();
        final byte[] mockImage = new byte[0];

        given(converterImageService.generateJpeg(foundImageBands, "T0nullnull_UNKNOWN_null.null"))
                .willReturn(mockImage);

        final byte[] response = processImageService.processGridReference(new GridReference());
        assertEquals(0, response.length);
    }

    @Test
    void given_valid_grid_reference_return_generated_byte_response() {

        final GridReference gridReference = new GridReference();

        gridReference.setUtmZone(33);
        gridReference.setLatitudeBand("U");
        gridReference.setGridSquare("UP");
        gridReference.setDate(LocalDate.of(2019, 2, 25));
        gridReference.setChannelMap("visible");

        final List<String> foundB2Images = new LinkedList<>();

        foundB2Images.add("T33UUP_20190225T100009_B02.tif");
        foundB2Images.add("T33UUP_20190225T110009_B02.tif");
        foundB2Images.add("T33UUP_20190225T120009_B02.tif");

        final List<String> foundB3Images = new LinkedList<>();

        foundB2Images.add("T33UUP_20190225T100009_B03.tif");
        foundB2Images.add("T33UUP_20190225T110009_B03.tif");
        foundB2Images.add("T33UUP_20190225T120009_B03.tif");

        final List<String> foundB4Images = new LinkedList<>();

        foundB2Images.add("T33UUP_20190225T100009_B04.tif");
        foundB2Images.add("T33UUP_20190225T110009_B04.tif");
        foundB2Images.add("T33UUP_20190225T120009_B04.tif");

        given(geoImageDataService.searchTiffImageKeyByPartialName("T33UUP_20190225T\\d{6}_B02.tif"))
                .willReturn(foundB2Images);

        given(geoImageDataService.searchTiffImageKeyByPartialName("T33UUP_20190225T\\d{6}_B03.tif"))
                .willReturn(foundB3Images);

        given(geoImageDataService.searchTiffImageKeyByPartialName("T33UUP_20190225T\\d{6}_B04.tif"))
                .willReturn(foundB4Images);

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        foundImageBands.put("B02", foundB2Images);
        foundImageBands.put("B03", foundB3Images);
        foundImageBands.put("B04", foundB4Images);

        final byte[] mockImage = new byte[20];
        new Random().nextBytes(mockImage);

        given(converterImageService.generateJpeg(foundImageBands, "T33UUP_20190225_visible.jpeg"))
                .willReturn(mockImage);

        ReflectionTestUtils.setField(processImageService, "outputFormat", "jpeg");

        final byte[] imageResponse = processImageService.processGridReference(gridReference);
        assertEquals(mockImage, imageResponse);
    }

    @Test
    void given_valid_grid_reference_return_cache_byte_response() {

        final String expectedCacheFile = "T33UUP_20190225_visible.jpeg";
        final GridReference gridReference = new GridReference();

        gridReference.setUtmZone(33);
        gridReference.setLatitudeBand("U");
        gridReference.setGridSquare("UP");
        gridReference.setDate(LocalDate.of(2019, 2, 25));
        gridReference.setChannelMap("visible");

        final List<String> foundList = new LinkedList<>();
        foundList.add(expectedCacheFile);

        given(geoImageDataService.searchTiffImageKeyInLocal(expectedCacheFile))
                .willReturn(foundList);

        final byte[] mockImage = new byte[20];
        new Random().nextBytes(mockImage);

        given(converterImageService.readLocalFile(expectedCacheFile))
                .willReturn(mockImage);

        ReflectionTestUtils.setField(processImageService, "outputFormat", "jpeg");

        final byte[] imageResponse = processImageService.processGridReference(gridReference);
        assertEquals(mockImage, imageResponse);
    }

}
