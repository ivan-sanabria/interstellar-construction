/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.interstellar.construction.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class to handle test cases for converter image service operations:
 *
 * <ul>
 *  <li> generate jpeg
 *  <li> read local file
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@ExtendWith(SpringExtension.class)
class ConverterImageServiceTest {

    /**
     * Define marker for root folder to look for local files in tests.
     */
    private static final String ROOT_MARKER = ".";

    /**
     * Mock of Converter Image Service.
     */
    private ConverterImageService converterImageService;

    /**
     * Init the configuration class before running test cases.
     */
    @BeforeEach
    void initBean() {
        converterImageService = new ConverterImageService();
    }


    @Test
    void given_null_parameters_return_empty_byte_array() {

        final byte[] results = converterImageService.generateJpeg(null, null);
        assertEquals(0, results.length);
    }

    @Test
    void given_empty_map_with_null_filename_return_empty_byte_array() {

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        final byte[] results = converterImageService.generateJpeg(foundImageBands, null);
        assertEquals(0, results.length);
    }

    @Test
    void given_empty_map_with_empty_filename_return_empty_byte_array() {

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        final byte[] results = converterImageService.generateJpeg(foundImageBands, "");
        assertEquals(0, results.length);
    }

    @Test
    void given_empty_map_with_invalid_filename_return_empty_byte_array() {

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();
        final String result = "result-without-extension";

        final byte[] results = converterImageService.generateJpeg(foundImageBands, result);
        assertEquals(0, results.length);
    }

    @Test
    void given_empty_map_with_valid_filename_return_empty_byte_array() {

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();
        final String result = "result.jpeg";

        final byte[] results = converterImageService.generateJpeg(foundImageBands, result);
        assertEquals(0, results.length);
    }

    @Test
    void given_map_one_invalid_key_item_with_null_filename_return_empty_byte_array() {

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        foundImageBands.put("This is not a valid key",
                Collections.singletonList("./images/test-c.jpg"));

        final byte[] results = converterImageService.generateJpeg(foundImageBands, null);
        assertEquals(0, results.length);
    }

    @Test
    void given_map_one_invalid_value_item_with_null_filename_return_empty_byte_array() {

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        foundImageBands.put("B02",
                Collections.singletonList("this is a real geo tiff file image?"));

        final byte[] results = converterImageService.generateJpeg(foundImageBands, null);
        assertEquals(0, results.length);
    }

    @Test
    void given_map_one_invalid_key_item_with_empty_filename_return_empty_byte_array() {

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        foundImageBands.put("This is not a valid key",
                Collections.singletonList("./images/test-c.jpg"));

        final byte[] results = converterImageService.generateJpeg(foundImageBands, "");
        assertEquals(0, results.length);
    }

    @Test
    void given_map_one_invalid_value_item_with_empty_filename_return_empty_byte_array() {

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        foundImageBands.put("B02",
                Collections.singletonList("this is a real geo tiff file image?"));

        final byte[] results = converterImageService.generateJpeg(foundImageBands, "");
        assertEquals(0, results.length);
    }

    @Test
    void given_map_one_invalid_key_item_with_invalid_filename_return_empty_byte_array() {

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        foundImageBands.put("This is not a valid key",
                Collections.singletonList("./images/test-c.jpg"));

        final String result = "result-with-out-extension";

        final byte[] results = converterImageService.generateJpeg(foundImageBands, result);
        assertEquals(0, results.length);
    }

    @Test
    void given_map_one_invalid_value_item_with_invalid_filename_return_empty_byte_array() {

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        foundImageBands.put("B02",
                Collections.singletonList("this is a real geo tiff file image?"));

        final String result = "result-with-out-extension";

        final byte[] results = converterImageService.generateJpeg(foundImageBands, result);
        assertEquals(0, results.length);
    }

    @Test
    void given_map_one_invalid_key_item_with_valid_filename_return_empty_byte_array() {

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        foundImageBands.put("This is not a valid key",
                Collections.singletonList("./images/test-c.jpg"));

        final String result = "result.jpeg";

        final byte[] results = converterImageService.generateJpeg(foundImageBands, result);
        assertEquals(0, results.length);
    }

    @Test
    void given_map_one_invalid_value_item_with_valid_filename_return_empty_byte_array() {

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        foundImageBands.put("B02",
                Collections.singletonList("this is a real geo tiff file image?"));

        final String result = "result.jpeg";

        final byte[] results = converterImageService.generateJpeg(foundImageBands, result);
        assertEquals(0, results.length);
    }

    @Test
    void given_map_one_valid_item_with_null_filename_return_empty_byte_array() throws IOException {

        final File fileA = new File("./images/test-a.jpg");
        final File fileC = new File("./images/test-c.jpg");

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        foundImageBands.put("B02",
                Collections.singletonList("./images/test-c.jpg"));

        ReflectionTestUtils.setField(converterImageService, "outputFormat", "jpeg");
        ReflectionTestUtils.setField(converterImageService, "localPath", ROOT_MARKER);

        ReflectionTestUtils.setField(converterImageService, "localEnabled", false);

        final byte[] resultDeleted = converterImageService.generateJpeg(foundImageBands, null);
        assertEquals(0, resultDeleted.length);

        Files.copy(fileA.toPath(), fileC.toPath());

        ReflectionTestUtils.setField(converterImageService, "localEnabled", true);

        final byte[] resultNotDeleted = converterImageService.generateJpeg(foundImageBands, null);

        assertEquals(0, resultNotDeleted.length);
        assertTrue(fileC.exists());
        assertTrue(fileC.delete());
    }

    @Test
    void given_map_one_valid_item_with_empty_filename_return_empty_byte_array() throws IOException {

        final File fileA = new File("./images/test-a.jpg");
        final File fileC = new File("./images/test-c.jpg");

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        foundImageBands.put("B03",
                Collections.singletonList("./images/test-c.jpg"));

        ReflectionTestUtils.setField(converterImageService, "outputFormat", "jpeg");
        ReflectionTestUtils.setField(converterImageService, "localPath", ROOT_MARKER);

        ReflectionTestUtils.setField(converterImageService, "localEnabled", false);

        final byte[] resultDeleted = converterImageService.generateJpeg(foundImageBands, "");
        assertEquals(0, resultDeleted.length);

        Files.copy(fileA.toPath(), fileC.toPath());

        ReflectionTestUtils.setField(converterImageService, "localEnabled", true);

        final byte[] resultNotDeleted = converterImageService.generateJpeg(foundImageBands, "");

        assertEquals(0, resultNotDeleted.length);
        assertTrue(fileC.exists());
        assertTrue(fileC.delete());
    }

    @Test
    void given_map_one_valid_item_with_invalid_filename_return_byte_array() throws IOException {

        final File fileA = new File("./images/test-a.jpg");
        final File fileC = new File("./images/test-c.jpg");

        Files.copy(fileA.toPath(), fileC.toPath());

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        foundImageBands.put("B04",
                Collections.singletonList("./images/test-c.jpg"));

        final String expectedFilename = "the-filename-is-irrelevant";
        final File expectedFile = new File(expectedFilename);

        ReflectionTestUtils.setField(converterImageService, "outputFormat", "jpeg");
        ReflectionTestUtils.setField(converterImageService, "localPath", ROOT_MARKER);

        ReflectionTestUtils.setField(converterImageService, "localEnabled", false);

        final byte[] resultDeleted = converterImageService.generateJpeg(foundImageBands, expectedFilename);

        assertNotNull(resultDeleted);
        assertFalse(fileC.exists());
        assertTrue(expectedFile.exists());
        assertTrue(expectedFile.delete());

        Files.copy(fileA.toPath(), fileC.toPath());

        ReflectionTestUtils.setField(converterImageService, "localEnabled", true);

        final byte[] resultNotDeleted = converterImageService.generateJpeg(foundImageBands, expectedFilename);

        assertNotNull(resultNotDeleted);
        assertTrue(fileC.exists());
        assertTrue(fileC.delete());
        assertTrue(expectedFile.exists());
        assertTrue(expectedFile.delete());
    }

    @Test
    void given_map_one_valid_item_with_valid_filename_return_byte_array() throws IOException {

        final File fileA = new File("./images/test-a.jpg");
        final File fileC = new File("./images/test-c.jpg");

        Files.copy(fileA.toPath(), fileC.toPath());

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        foundImageBands.put("B02",
                Collections.singletonList("./images/test-c.jpg"));

        final String expectedFilename = "result.jpeg";
        final File expectedFile = new File(expectedFilename);

        ReflectionTestUtils.setField(converterImageService, "outputFormat", "jpeg");
        ReflectionTestUtils.setField(converterImageService, "localPath", ROOT_MARKER);

        ReflectionTestUtils.setField(converterImageService, "localEnabled", false);

        final byte[] resultDeleted = converterImageService.generateJpeg(foundImageBands, expectedFilename);

        assertNotNull(resultDeleted);
        assertFalse(fileC.exists());
        assertTrue(expectedFile.exists());
        assertTrue(expectedFile.delete());

        Files.copy(fileA.toPath(), fileC.toPath());

        ReflectionTestUtils.setField(converterImageService, "localEnabled", true);

        final byte[] resultNotDeleted = converterImageService.generateJpeg(foundImageBands, expectedFilename);

        assertNotNull(resultNotDeleted);
        assertTrue(fileC.exists());
        assertTrue(fileC.delete());
        assertTrue(expectedFile.exists());
        assertTrue(expectedFile.delete());
    }

    @Test
    void given_map_three_items_one_invalid_key_item_with_valid_filename_return_byte_array() throws IOException {

        final File fileA = new File("./images/test-a.jpg");
        final File fileC = new File("./images/test-c.jpg");

        Files.copy(fileA.toPath(), fileC.toPath());

        final List<String> filenames = new LinkedList<>();

        filenames.add("./images/test-c.jpg");

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        foundImageBands.put("B02", filenames);
        foundImageBands.put("B03", filenames);
        foundImageBands.put("This is not supported", filenames);

        final String expectedFilename = "result.jpeg";
        final File expectedFile = new File(expectedFilename);

        ReflectionTestUtils.setField(converterImageService, "outputFormat", "jpeg");
        ReflectionTestUtils.setField(converterImageService, "localPath", ROOT_MARKER);

        ReflectionTestUtils.setField(converterImageService, "localEnabled", false);

        final byte[] resultDeleted = converterImageService.generateJpeg(foundImageBands, expectedFilename);

        assertNotNull(resultDeleted);
        assertFalse(fileC.exists());
        assertTrue(expectedFile.exists());
        assertTrue(expectedFile.delete());

        Files.copy(fileA.toPath(), fileC.toPath());

        ReflectionTestUtils.setField(converterImageService, "localEnabled", true);

        final byte[] resultNotDeleted = converterImageService.generateJpeg(foundImageBands, expectedFilename);

        assertNotNull(resultNotDeleted);
        assertTrue(fileC.exists());
        assertTrue(fileC.delete());
        assertTrue(expectedFile.exists());
        assertTrue(expectedFile.delete());
    }

    @Test
    void given_map_three_items_one_invalid_value_item_with_valid_filename_return_byte_array() throws IOException {

        final File fileA = new File("./images/test-a.jpg");
        final File fileC = new File("./images/test-c.jpg");

        Files.copy(fileA.toPath(), fileC.toPath());

        final List<String> filenames = new LinkedList<>();

        filenames.add("./images/test-c.jpg");
        filenames.add("./images/invalid-file.jpg");

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        foundImageBands.put("B02", filenames);
        foundImageBands.put("B03", filenames);
        foundImageBands.put("B04", filenames);

        final String expectedFilename = "result.jpeg";
        final File expectedFile = new File(expectedFilename);

        ReflectionTestUtils.setField(converterImageService, "outputFormat", "jpeg");
        ReflectionTestUtils.setField(converterImageService, "localPath", ROOT_MARKER);

        ReflectionTestUtils.setField(converterImageService, "localEnabled", false);

        final byte[] resultDeleted = converterImageService.generateJpeg(foundImageBands, expectedFilename);

        assertNotNull(resultDeleted);
        assertFalse(fileC.exists());
        assertTrue(expectedFile.exists());
        assertTrue(expectedFile.delete());

        Files.copy(fileA.toPath(), fileC.toPath());

        ReflectionTestUtils.setField(converterImageService, "localEnabled", true);

        final byte[] resultNotDeleted = converterImageService.generateJpeg(foundImageBands, expectedFilename);

        assertNotNull(resultNotDeleted);
        assertTrue(fileC.exists());
        assertTrue(fileC.delete());
        assertTrue(expectedFile.exists());
        assertTrue(expectedFile.delete());
    }

    @Test
    void given_map_three_items_with_valid_filename_return_byte_array() throws IOException {

        final File fileA = new File("./images/test-a.jpg");
        final File fileC = new File("./images/test-c.jpg");

        Files.copy(fileA.toPath(), fileC.toPath());

        final File fileB = new File("./images/test-b.jpg");
        final File fileD = new File("./images/test-d.jpg");

        Files.copy(fileB.toPath(), fileD.toPath());

        final List<String> filenames = new LinkedList<>();

        filenames.add("./images/test-c.jpg");
        filenames.add("./images/test-d.jpg");

        final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

        foundImageBands.put("B02", filenames);
        foundImageBands.put("B03", filenames);
        foundImageBands.put("B04", filenames);

        final String expectedFilename = "result.jpeg";
        final File expectedFile = new File(expectedFilename);

        ReflectionTestUtils.setField(converterImageService, "outputFormat", "jpeg");
        ReflectionTestUtils.setField(converterImageService, "localPath", ROOT_MARKER);

        ReflectionTestUtils.setField(converterImageService, "localEnabled", false);

        final byte[] resultDeleted = converterImageService.generateJpeg(foundImageBands, expectedFilename);

        assertNotNull(resultDeleted);
        assertFalse(fileC.exists());
        assertFalse(fileD.exists());
        assertTrue(expectedFile.exists());
        assertTrue(expectedFile.delete());

        Files.copy(fileA.toPath(), fileC.toPath());
        Files.copy(fileB.toPath(), fileD.toPath());

        ReflectionTestUtils.setField(converterImageService, "localEnabled", true);

        final byte[] resultNotDeleted = converterImageService.generateJpeg(foundImageBands, expectedFilename);

        assertNotNull(resultNotDeleted);
        assertTrue(fileC.exists());
        assertTrue(fileC.delete());
        assertTrue(fileD.exists());
        assertTrue(fileD.delete());
        assertTrue(expectedFile.exists());
        assertTrue(expectedFile.delete());
    }

    @Test
    void given_null_filename_return_empty_byte_array() {

        final byte[] results = converterImageService.readLocalFile(null);
        assertEquals(0, results.length);
    }

    @Test
    void given_empty_filename_return_empty_byte_array() {

        final byte[] results = converterImageService.readLocalFile("");
        assertEquals(0, results.length);
    }

    @Test
    void given_invalid_filename_return_empty_byte_array() {

        final byte[] results = converterImageService.readLocalFile("this-does-not-exists");
        assertEquals(0, results.length);
    }

    @Test
    void given_valid_filename_return_byte_array() {

        final byte[] results = converterImageService.readLocalFile("./images/test-a.jpg");
        assertNotNull(results);
    }

}
