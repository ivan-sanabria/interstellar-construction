/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.interstellar.construction.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.mock;

/**
 * Class to handle test cases for geo image data service operations:
 *
 * <ul>
 *  <li> search tiff image key by partial name
 *  <li> search tiff image key in local
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@ExtendWith(SpringExtension.class)
class GeoImageDataServiceTest {

    /**
     * Define marker for root folder to look for local files in tests.
     */
    private static final String ROOT_MARKER = ".";

    /**
     * Mock amazon S3 client.
     */
    @MockBean
    private AmazonS3 interstellarS3Client;

    /**
     * Mock of Geo Image Service.
     */
    private GeoImageDataService geoImageDataService;

    /**
     * Init the necessary mocks before running test cases.
     */
    @BeforeEach
    void initBean() {
        geoImageDataService = new GeoImageDataService(interstellarS3Client);
    }


    @Test
    void given_s3_key_return_empty_result_keys_with_list_amazon_exception() {

        final String bucketName = "test-bucket";
        final String key = "file-key.txt";

        given(interstellarS3Client.listObjectsV2(bucketName))
                .willThrow(AmazonServiceException.class);

        ReflectionTestUtils.setField(geoImageDataService, "localEnabled", false);
        ReflectionTestUtils.setField(geoImageDataService, "bucketName", bucketName);

        final List<String> resultS3Keys = geoImageDataService.searchTiffImageKeyByPartialName(key);
        assertTrue(resultS3Keys.isEmpty());

        final File file = new File(ROOT_MARKER, key);
        assertFalse(file.exists());
    }

    @Test
    void given_s3_key_return_empty_result_keys_with_get_amazon_exception() {

        final String bucketName = "test-bucket";
        final String key = "file-key.txt";

        final ListObjectsV2Result results = mock(ListObjectsV2Result.class);
        final S3ObjectSummary mockSummary = mock(S3ObjectSummary.class);

        given(interstellarS3Client.listObjectsV2(bucketName))
                .willReturn(results);

        given(results.getObjectSummaries())
                .willReturn(Collections.singletonList(mockSummary));

        given(mockSummary.getKey())
                .willReturn(key);

        final S3Object s3Object = mock(S3Object.class);

        given(interstellarS3Client.getObject(bucketName, key))
                .willReturn(s3Object);

        given(s3Object.getObjectContent())
                .willThrow(AmazonServiceException.class);

        ReflectionTestUtils.setField(geoImageDataService, "localEnabled", false);
        ReflectionTestUtils.setField(geoImageDataService, "bucketName", bucketName);

        final List<String> resultS3Keys = geoImageDataService.searchTiffImageKeyByPartialName(key);
        assertTrue(resultS3Keys.isEmpty());

        final File file = new File(ROOT_MARKER, key);
        assertFalse(file.exists());
    }

    @Test
    void given_s3_key_return_empty_result_with_io_exception() throws IOException {

        final String bucketName = "test-bucket";
        final String key = "file-key.txt";

        final ListObjectsV2Result results = mock(ListObjectsV2Result.class);
        final S3ObjectSummary mockSummary = mock(S3ObjectSummary.class);

        given(interstellarS3Client.listObjectsV2(bucketName))
                .willReturn(results);

        given(results.getObjectSummaries())
                .willReturn(Collections.singletonList(mockSummary));

        given(mockSummary.getKey())
                .willReturn(key);

        final S3Object s3Object = mock(S3Object.class);
        final S3ObjectInputStream inputStream = mock(S3ObjectInputStream.class);

        given(interstellarS3Client.getObject(bucketName, key))
                .willReturn(s3Object);

        given(s3Object.getObjectContent())
                .willReturn(inputStream);

        given(inputStream.read(any()))
                .willThrow(IOException.class);

        ReflectionTestUtils.setField(geoImageDataService, "localEnabled", false);
        ReflectionTestUtils.setField(geoImageDataService, "bucketName", bucketName);

        final List<String> resultS3Keys = geoImageDataService.searchTiffImageKeyByPartialName(key);
        assertTrue(resultS3Keys.isEmpty());

        final File file = new File(ROOT_MARKER, key);
        assertFalse(file.exists());
    }

    @Test
    void given_s3_key_return_empty_result_keys_with_not_exception() {

        final String bucketName = "test-bucket";
        final String key = "file-key-\\d{6}-B02.txt";
        final String otherKey = "other-file-key-100031-B02.txt";

        final ListObjectsV2Result results = mock(ListObjectsV2Result.class);
        final S3ObjectSummary mockSummary = mock(S3ObjectSummary.class);

        given(interstellarS3Client.listObjectsV2(bucketName))
                .willReturn(results);

        given(results.getObjectSummaries())
                .willReturn(Collections.singletonList(mockSummary));

        given(mockSummary.getKey())
                .willReturn(otherKey);

        ReflectionTestUtils.setField(geoImageDataService, "localEnabled", false);
        ReflectionTestUtils.setField(geoImageDataService, "bucketName", bucketName);

        final List<String> resultS3Keys = geoImageDataService.searchTiffImageKeyByPartialName(key);
        assertTrue(resultS3Keys.isEmpty());

        final File file = new File(ROOT_MARKER, key);
        assertFalse(file.exists());
    }

    @Test
    void given_local_key_return_empty_result_keys_with_not_exception() {

        final String key = "file-key-\\d{6}-B02.txt";

        ReflectionTestUtils.setField(geoImageDataService, "localEnabled", true);
        ReflectionTestUtils.setField(geoImageDataService, "localPath", ROOT_MARKER);

        final List<String> resultLocalKeys = geoImageDataService.searchTiffImageKeyByPartialName(key);
        assertTrue(resultLocalKeys.isEmpty());

        ReflectionTestUtils.setField(geoImageDataService, "localPath", "this is not a folder");

        final List<String> resultKeys = geoImageDataService.searchTiffImageKeyByPartialName(key);
        assertTrue(resultKeys.isEmpty());
    }

    @Test
    void given_s3_key_return_one_result() {

        final String bucketName = "test-bucket";
        final String key = "file-key-\\d{6}-B02.txt";
        final String validKey = "file-key-100031-B02.txt";

        final ListObjectsV2Result results = mock(ListObjectsV2Result.class);
        final S3ObjectSummary mockSummary = mock(S3ObjectSummary.class);
        final S3Object s3Object = mock(S3Object.class);

        given(interstellarS3Client.listObjectsV2(bucketName))
                .willReturn(results);

        given(results.getObjectSummaries())
                .willReturn(Collections.singletonList(mockSummary));

        given(mockSummary.getKey())
                .willReturn(validKey);

        final byte[] testBytes = "this is a test".getBytes();
        final InputStream byteArrayInputStream = new ByteArrayInputStream(testBytes);
        final S3ObjectInputStream inputStream = new S3ObjectInputStream(byteArrayInputStream, null);

        given(interstellarS3Client.getObject(bucketName, validKey))
                .willReturn(s3Object);

        given(s3Object.getObjectContent())
                .willReturn(inputStream);

        ReflectionTestUtils.setField(geoImageDataService, "localEnabled", false);
        ReflectionTestUtils.setField(geoImageDataService, "bucketName", bucketName);
        ReflectionTestUtils.setField(geoImageDataService, "localPath", ROOT_MARKER);

        final List<String> resultS3Keys = geoImageDataService.searchTiffImageKeyByPartialName(key);
        assertEquals(1, resultS3Keys.size());

        final File file = new File(ROOT_MARKER, validKey);

        assertTrue(file.exists());
        assertTrue(file.delete());
    }

    @Test
    void given_local_key_return_one_result() throws IOException {

        final String key = "file-key-\\d{6}-B02.txt";
        final String validKey = "file-key-100031-B02.txt";

        ReflectionTestUtils.setField(geoImageDataService, "localEnabled", true);
        ReflectionTestUtils.setField(geoImageDataService, "localPath", ROOT_MARKER);

        final List<String> lines = Arrays.asList("The first line", "The second line");
        final Path localPathFile = Paths.get(validKey);

        Files.write(localPathFile, lines, StandardCharsets.UTF_8);

        final List<String> resultLocalKeys = geoImageDataService.searchTiffImageKeyByPartialName(key);
        assertEquals(1, resultLocalKeys.size());

        final File file = new File(ROOT_MARKER, validKey);

        assertTrue(file.exists());
        assertTrue(file.delete());
    }

}
