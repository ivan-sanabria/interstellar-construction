/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.interstellar.construction.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.interstellar.construction.dto.GridReference;
import com.interstellar.construction.service.ProcessImageService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Random;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Class to handle test cases for generate image controller operations:
 *
 * <ul>
 *  <li> generate jpeg image from the given grid reference
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(GenerateImageController.class)
class GenerateImageControllerTest {

    /**
     * Mock of Process Image Service.
     */
    @MockBean
    private ProcessImageService processImageService;

    /**
     * Mock of MVC to perform HTTP requests.
     */
    @Autowired
    private MockMvc mvc;

    /**
     * Object Mapper used to write JSON request data into content bodies.
     */
    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void given_grid_reference_return_not_found_image_response() throws Exception {

        final GridReference gridReference = new GridReference();

        gridReference.setUtmZone(33);
        gridReference.setLatitudeBand("U");
        gridReference.setGridSquare("UP");
        gridReference.setDate(LocalDate.of(2018, 2, 25));
        gridReference.setChannelMap("visible");

        given(processImageService.processGridReference(gridReference))
                .willReturn(new byte[0]);

        mvc.perform(post("/generate-image")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(gridReference)))
                .andExpect(status().isNotFound());

        given(processImageService.processGridReference(gridReference))
                .willReturn(new byte[0]);

        mvc.perform(post("/generate-image")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(gridReference)))
                .andExpect(status().isNotFound());
    }

    @Test
    void given_grid_reference_return_found_image_response() throws Exception {

        final GridReference gridReference = new GridReference();

        gridReference.setUtmZone(33);
        gridReference.setLatitudeBand("U");
        gridReference.setGridSquare("UP");
        gridReference.setDate(LocalDate.of(2018, 2, 25));
        gridReference.setChannelMap("visible");

        byte[] mockImage = new byte[20];
        new Random().nextBytes(mockImage);

        given(processImageService.processGridReference(gridReference))
                .willReturn(mockImage);

        mvc.perform(post("/generate-image")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(gridReference)))
                .andExpect(status().isOk());
    }

}
