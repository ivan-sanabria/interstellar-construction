/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.interstellar.construction.dto;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Class to handle test cases for grid reference operations:
 *
 * <ul>
 *  <li> equals
 *  <li> hashcode
 *  <li> toString
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
class GridReferenceTest {


    @Test
    void validate_equals_different_object_values() {

        final GridReference gridReferenceX = new GridReference(33, "U", "UP", LocalDate.now(), "visible");
        final GridReference gridReferenceY = new GridReference(34, "U", "DO", LocalDate.of(2019, 5, 12), "another");

        assertNotEquals(gridReferenceX, gridReferenceY);
        assertNotEquals(gridReferenceY, gridReferenceX);
    }

    @Test
    void validate_equals_same_object_values() {

        final GridReference gridReferenceX = new GridReference(33, "U", "UP", LocalDate.now(), "visible");
        final GridReference gridReferenceY = new GridReference(33, "U", "UP", LocalDate.now(), "visible");
        final GridReference gridReferenceZ = new GridReference(33, "U", "UP", LocalDate.now(), "visible");

        assertEquals(gridReferenceX, gridReferenceY);
        assertEquals(gridReferenceY, gridReferenceX);
        assertEquals(gridReferenceY, gridReferenceZ);
        assertEquals(gridReferenceX, gridReferenceZ);

        // Validate for consistency

        gridReferenceX.setChannelMap("waterVapor");
        gridReferenceY.setChannelMap("vegetation");

        assertNotEquals(gridReferenceX, gridReferenceY);
        assertNotEquals(gridReferenceY, gridReferenceX);
        assertNotEquals(gridReferenceY, gridReferenceZ);
        assertNotEquals(gridReferenceX, gridReferenceZ);
    }

    @Test
    void validate_equals_different_instances() {

        final GridReference gridReferenceX = new GridReference(33, "U", "UP", LocalDate.now(), "visible");
        final GridReference gridReferenceY = new GridReference();

        assertNotEquals(gridReferenceX, gridReferenceY);
        assertNotEquals(gridReferenceY, gridReferenceX);
    }

    @Test
    void validate_equals_same_instance() {

        final GridReference gridReferenceX = new GridReference(33, "U", "UP", LocalDate.now(), "visible");
        assertEquals(gridReferenceX, gridReferenceX);
    }

    @Test
    void validate_equals_atomic_reference_instance() {

        final GridReference gridReferenceX = new GridReference(33, "U", "UP", LocalDate.now(), "visible");
        final GridReference gridReferenceY = new AtomicReference<>(gridReferenceX).get();

        assertEquals(gridReferenceX, gridReferenceY);
        assertEquals(gridReferenceY, gridReferenceX);
    }

    @Test
    void validate_equals_different_class_instance() {

        final GridReference gridReferenceX = new GridReference(33, "U", "UP", LocalDate.now(), "visible");
        final StringBuilder testObject = new StringBuilder("hello word");

        assertNotEquals(gridReferenceX, testObject);
    }

    @Test
    void validate_grid_reference_in_set() {

        final Set<GridReference> gridReferenceSet = new HashSet<>();

        gridReferenceSet.add(
                new GridReference(33, "U", "UP", LocalDate.now(), "visible"));

        gridReferenceSet.add(
                new GridReference(33, "U", "UP", LocalDate.now(), "visible"));

        assertEquals(1, gridReferenceSet.size());
    }

    @Test
    void validate_hash_code_different_object_values() {

        final GridReference gridReferenceX = new GridReference(33, "U", "UP", LocalDate.now(), "visible");
        final GridReference gridReferenceY = new GridReference(34, "U", "DO", LocalDate.of(2019, 5, 12), "another");

        assertNotEquals(gridReferenceX.hashCode(), gridReferenceY.hashCode());
    }

    @Test
    void validate_hash_code_same_object_values() {

        final GridReference gridReferenceX = new GridReference(33, "U", "UP", LocalDate.now(), "visible");
        final GridReference gridReferenceY = new GridReference(33, "U", "UP", LocalDate.now(), "visible");

        assertEquals(gridReferenceX.hashCode(), gridReferenceY.hashCode());
    }

    @Test
    void validate_hash_code_different_instances() {

        final GridReference gridReferenceX = new GridReference(33, "U", "UP", LocalDate.now(), "visible");
        final GridReference gridReferenceY = new GridReference();

        assertNotEquals(gridReferenceX.hashCode(), gridReferenceY.hashCode());
    }

    @Test
    void validate_to_string_with_complete_object_values() {

        final String expected = "GridReference(utmZone=33, latitudeBand=U, gridSquare=UP, date=2019-02-26, channelMap=visible)";
        final GridReference gridReferenceX = new GridReference(33, "U", "UP", LocalDate.of(2019, 2, 26), "visible");

        assertEquals(expected, gridReferenceX.toString());
    }

    @Test
    void validate_to_string_with_null_object_values() {

        final String expected = "GridReference(utmZone=33, latitudeBand=null, gridSquare=UP, date=2019-02-26, channelMap=null)";
        final GridReference gridReferenceX = new GridReference(33, null, "UP", LocalDate.of(2019, 2, 26), null);

        assertEquals(expected, gridReferenceX.toString());
    }

    @Test
    void validate_to_string_with_empty_values() {

        final String expected = "GridReference(utmZone=0, latitudeBand=null, gridSquare=null, date=null, channelMap=null)";
        final GridReference gridReferenceX = new GridReference();

        assertEquals(expected, gridReferenceX.toString());
    }

}
