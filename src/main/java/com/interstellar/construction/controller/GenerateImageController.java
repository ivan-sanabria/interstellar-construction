/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.interstellar.construction.controller;

import com.interstellar.construction.dto.GridReference;
import com.interstellar.construction.service.ProcessImageService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class responsible of handling request made to /generate-image endpoint supporting:
 *
 * <ul>
 *  <li> generate jpeg image from the given grid reference
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@RestController
@Tag(name = "Generate Image Endpoint", description = "Supported operations to create images based on satellite data.")
public class GenerateImageController {

    /**
     * Service used to process image using the given grid reference.
     */
    private final ProcessImageService processImageService;

    /**
     * Constructor of generate image controller to inject the required services.
     *
     * @param processImageService Process image service instance used to transform the grid reference into jpeg image.
     */
    @Autowired
    public GenerateImageController(ProcessImageService processImageService) {
        this.processImageService = processImageService;
    }

    /**
     * Method to handle post requests made to /generate-image endpoint.
     *
     * @param gridReference Grid reference data to filter the search.
     * @return Response entity with the image found, otherwise NOT_FOUND and message is returned.
     */
    @Operation(description = "Retrieves the image for the given grid reference request.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Successfully build and retrieve the image.",
                    content = @Content(schema = @Schema(type = "string", format = "binary"), examples = @ExampleObject(name = "imageFile", summary = "Generated image for the given grid reference.", description = "Result for the given grid reference.", externalValue = "/images/response.jpeg"))),
            @ApiResponse(responseCode = "400",
                    description = "The given json is not parsable to a grid reference.",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "The grid reference requested was not found.",
                    content = @Content),
            @ApiResponse(responseCode = "415",
                    description = "Given content-type on the request was not correct. Expected is application/json.",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Server error occurs by processing the grid reference request.",
                    content = @Content)
    })
    @PostMapping(value = "/generate-image", produces = "image/jpeg")
    public ResponseEntity<byte[]> generateImage(
            @Parameter(description = "Grid reference to generate the response image.")
            @RequestBody GridReference gridReference) {

        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.IMAGE_JPEG);

        final byte[] imageResponse = processImageService.processGridReference(gridReference);

        if (0 == imageResponse.length)
            return new ResponseEntity<>(httpHeaders, HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(imageResponse, httpHeaders, HttpStatus.OK);
    }

}
