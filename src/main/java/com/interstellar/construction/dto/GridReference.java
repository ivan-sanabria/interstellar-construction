/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.interstellar.construction.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * Class to encapsulate the body supported by the endpoint /generate-image.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Grid reference used to generate image. For further information: " +
        "https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system")
public class GridReference {

    /**
     * Universal Transverse Mercator zone.
     */
    @Schema(description = "Universal Transverse Mercator Zone.",
            example = "33")
    private int utmZone;

    /**
     * Latitude band of the utm.
     */
    @Schema(description = "Latitude Bands specified by military grid reference system.",
            example = "U")
    private String latitudeBand;

    /**
     * Grid square of the utm.
     */
    @Schema(description = "Grid square used to locate exact area of the grid.",
            example = "UP")
    private String gridSquare;

    /**
     * Date to search for in ISO-Format.
     */
    @Schema(description = "Timestamp ISO format when the image was taken by the Sentinel-2 satellite.",
            example = "2018-08-04")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate date;

    /**
     * Channel map to display specific colors on response image.
     */
    @Schema(description = "Specifies which kind of image to return using specific RGB bands on image processing.",
            example = "visible")
    private String channelMap;

}
