/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.interstellar.construction.service;

import com.interstellar.construction.dto.GridReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Class responsible of orchestrating different services to merge images according to grid reference data by
 * supported operations:
 *
 * <ul>
 *  <li> process grid reference
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Service
public class ProcessImageService {

    /**
     * Define date formatter for S3 file search.
     */
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd");

    /**
     * Define supported image data set format for the service.
     */
    private static final String DATASET_FORMAT = "tif";

    /**
     * Inject geo image data service responsible of getting the data from S3 bucket.
     */
    private final GeoImageDataService geoImageDataService;

    /**
     * Inject converter image service responsible of getting jpeg image from given local file.
     */
    private final ConverterImageService converterImageService;

    /**
     * Define supported response format for the service.
     */
    @Value("${dataProperties.outputFormat}")
    private String outputFormat;

    /**
     * Constructor of process image service to inject required services.
     *
     * @param geoImageDataService   Geo image data service instance used to get the tiff images from datasource.
     * @param converterImageService Convert image service instance used to convert multiple tiff images into jpeg image.
     */
    @Autowired
    public ProcessImageService(GeoImageDataService geoImageDataService, ConverterImageService converterImageService) {
        this.geoImageDataService = geoImageDataService;
        this.converterImageService = converterImageService;
    }

    /**
     * Process the given grid reference and return build image based on the given color map.
     *
     * @param gridReference Grid reference used to build image.
     * @return Byte array that contains the jpeg image build by the service.
     */
    public byte[] processGridReference(final GridReference gridReference) {

        if (null == gridReference)
            return new byte[0];

        final String filename = buildOutputFilename(gridReference);
        final List<String> cacheLocalImages = geoImageDataService.searchTiffImageKeyInLocal(filename);

        if (cacheLocalImages.isEmpty()) {

            final String channelMap = gridReference.getChannelMap();

            final List<String> supportedBands = getSupportedChannels()
                    .getOrDefault(channelMap, Collections.emptyList());

            final Map<String, List<String>> foundImageBands = new LinkedHashMap<>();

            for (String band : supportedBands) {

                final String partialName = buildSearchKey(gridReference, band);
                final List<String> partialFoundKeys = geoImageDataService.searchTiffImageKeyByPartialName(partialName);

                foundImageBands.put(band, partialFoundKeys);
            }

            return converterImageService.generateJpeg(foundImageBands, filename);

        } else {

            final String localFilename = cacheLocalImages.get(0);

            return converterImageService.readLocalFile(localFilename);
        }
    }

    /**
     * Build the output filename using grid reference data.
     *
     * @param gridReference Grid reference data to filter the search.
     * @return String representing the output filename of the service. Example: T33UUP_20180804_visible
     */
    private String buildOutputFilename(final GridReference gridReference) {

        final LocalDate gridReferenceDate = gridReference.getDate();
        final String generatedDate = (null == gridReferenceDate) ? "UNKNOWN" : FORMATTER.format(gridReferenceDate);

        return "T" + gridReference.getUtmZone()
                + gridReference.getLatitudeBand()
                + gridReference.getGridSquare()
                + "_"
                + generatedDate
                + "_"
                + gridReference.getChannelMap()
                + "."
                + outputFormat;
    }

    /**
     * Build the search key term to get list of images from S3.
     *
     * @param gridReference Grid reference data to filter the search.
     * @param band          Band specified according to channels map.
     * @return String representing the search key term. Example: T33UUP_20180804100003_B02.tif
     */
    private String buildSearchKey(final GridReference gridReference, final String band) {

        return "T" + gridReference.getUtmZone()
                + gridReference.getLatitudeBand()
                + gridReference.getGridSquare()
                + "_"
                + FORMATTER.format(gridReference.getDate())
                + "T\\d{6}"
                + "_"
                + band
                + "."
                + DATASET_FORMAT;
    }

    /**
     * Retrieves the supported channels of the process image service.
     *
     * @return A Map instance with the supported channels of the process image service.
     */
    private Map<String, List<String>> getSupportedChannels() {

        final Map<String, List<String>> channels = new HashMap<>();

        channels.put("visible",
                Arrays.asList("B02", "B03", "B04"));

        channels.put("vegetation",
                Arrays.asList("B05", "B06", "B07"));

        channels.put("waterVapor",
                Collections.singletonList("B09"));

        return channels;
    }

}
