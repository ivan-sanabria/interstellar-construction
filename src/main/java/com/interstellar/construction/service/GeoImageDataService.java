/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.interstellar.construction.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class responsible of handling request against S3 bucket and supporting operations:
 *
 * <ul>
 *  <li> search tiff image key by partial name
 *  <li> search tiff image key in local
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Service
public class GeoImageDataService {

    /**
     * Define logger instance to track errors.
     */
    private static final Logger LOG = LoggerFactory.getLogger(GeoImageDataService.class);

    /**
     * Define the S3 client to request tiff images from bucket.
     */
    private final AmazonS3 interstellarS3Client;

    /**
     * Boolean that enable or disable S3 as image data provider.
     */
    @Value("${dataProperties.localEnabled}")
    private boolean localEnabled;

    /**
     * Local path where tiff images are stored.
     */
    @Value("${dataProperties.localDirectory}")
    private String localPath;

    /**
     * Bucket name where the tiff images are stored.
     */
    @Value("${amazonProperties.bucketName}")
    private String bucketName;

    /**
     * Constructor of geo image data service to inject required services.
     *
     * @param interstellarS3Client Amazon S3 client used to get and search images on S3 bucket.
     */
    @Autowired
    public GeoImageDataService(AmazonS3 interstellarS3Client) {
        this.interstellarS3Client = interstellarS3Client;
    }

    /**
     * Validate that the given partial name exists on some key names in supported datasource.
     *
     * @param partialName Partial name of the tiff image file to search in supported datasource.
     * @return A List with key names found on the datasource.
     */
    List<String> searchTiffImageKeyByPartialName(final String partialName) {

        if (localEnabled) {

            return searchTiffImageKeyInLocal(partialName);

        } else {

            final List<String> foundKeys = searchTiffImageKeyInS3(partialName);
            final List<String> localKeys = new ArrayList<>();

            for (String foundKey : foundKeys) {

                final String localNameTiff = getTiffImageByS3Key(foundKey);

                if (null != localNameTiff)
                    localKeys.add(localNameTiff);
            }

            return localKeys;
        }
    }

    /**
     * Search the given partial name exists on S3 bucket.
     *
     * @param partialName Partial name of the tiff image file to search.
     * @return A List with key names found on local system.
     */
    List<String> searchTiffImageKeyInLocal(final String partialName) {

        final List<String> foundKeys = new ArrayList<>();

        final File localDirectory = new File(localPath);
        final File[] files = localDirectory.listFiles();

        if (null == files)
            return foundKeys;

        for (File file : files) {

            final String filename = file.getName();

            if (file.isFile() && filename.matches(partialName)) {
                final String path = file.getPath();
                foundKeys.add(path);
            }
        }

        return foundKeys;
    }

    /**
     * Downloads the image from S3 and stored in local file.
     *
     * @param name Name of the file to download from S3.
     * @return Local file name.
     */
    private String getTiffImageByS3Key(final String name) {

        final File localFile = new File(localPath, name);

        try (FileOutputStream outputStream = new FileOutputStream(localFile)) {

            final S3Object s3Object = interstellarS3Client.getObject(bucketName, name);
            final S3ObjectInputStream inputStream = s3Object.getObjectContent();

            byte[] readBuffer = new byte[1024];
            int readLength;

            while ((readLength = inputStream.read(readBuffer)) > 0) {
                outputStream.write(readBuffer, 0, readLength);
            }

            inputStream.close();

            return localFile.getPath();

        } catch (AmazonServiceException ase) {

            LOG.error("Error downloading image with name {} from Amazon S3 - ", name, ase);

        } catch (IOException ioe) {

            LOG.error("Error writing image with name {} in local system - ", name, ioe);

        }

        LOG.info("Deleting corrupted image with name {} in local system {}.", name, localFile.delete());

        return null;
    }

    /**
     * Search the given partial name exists on S3 bucket.
     *
     * @param partialName Partial name of the TIFF image file to search.
     * @return A List with key names found on S3 bucket.
     */
    private List<String> searchTiffImageKeyInS3(final String partialName) {

        final List<String> foundKeys = new ArrayList<>();

        try {

            final ListObjectsV2Result result = interstellarS3Client.listObjectsV2(bucketName);
            final List<S3ObjectSummary> objects = result.getObjectSummaries();

            for (S3ObjectSummary os : objects) {

                final String currentKey = os.getKey();

                if (currentKey.matches(partialName))
                    foundKeys.add(currentKey);
            }

        } catch (AmazonServiceException ase) {

            LOG.error("Error listing images with partial name {} from Amazon S3 - ", partialName, ase);
        }

        return foundKeys;
    }

}
