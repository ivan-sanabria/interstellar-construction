/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.interstellar.construction.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class responsible of converting tiff images into jpeg by supporting operations:
 *
 * <ul>
 *  <li> generate jpeg
 *  <li> read local file
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Service
public class ConverterImageService {

    /**
     * Define logger instance to track errors.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ConverterImageService.class);

    /**
     * Boolean that enable or disable S3 as image data provider.
     */
    @Value("${dataProperties.localEnabled}")
    private boolean localEnabled;

    /**
     * Local path where tiff images are stored.
     */
    @Value("${dataProperties.localDirectory}")
    private String localPath;

    /**
     * Define supported response format for the service.
     */
    @Value("${dataProperties.outputFormat}")
    private String outputFormat;

    /**
     * Generate jpeg image for the given filenames.
     *
     * @param foundImageBands A Map with key : supported band and value: list of filenames of the tiff images.
     * @param filename        Filename given to the output file.
     * @return A byte array of the jpeg image. In case of failure on the process the method return null.
     */
    byte[] generateJpeg(final Map<String, List<String>> foundImageBands, final String filename) {

        if (null == foundImageBands || foundImageBands.isEmpty())
            return new byte[0];

        if (null == filename || filename.isEmpty())
            return new byte[0];

        final BufferedImage mergeImage = buildMergeImage(foundImageBands);

        if (!localEnabled) {

            foundImageBands.forEach((sensorBand, images) -> {

                for (String currentFilename : images) {

                    try {

                        final Path localFile = Path.of(currentFilename);

                        Files.delete(localFile);

                    } catch (IOException ioe) {

                        LOG.error("Error deleting file {} - ", currentFilename, ioe);
                    }

                    LOG.info("Deletion of file {} is successful for sensor band {}.", currentFilename, sensorBand);
                }
            });
        }

        return getJpegImageBytes(mergeImage, filename);
    }

    /**
     * Retrieves the bytes of the given filename.
     *
     * @param filename Filename of local file to read bytes.
     * @return Byte array of the given filename. In case the file doesn't exists the method return null.
     */
    byte[] readLocalFile(final String filename) {

        if (null == filename)
            return new byte[0];

        try {

            final Path localFile = Path.of(filename);

            return Files.readAllBytes(localFile);

        } catch (IOException ioe) {

            LOG.error("Error reading local file {} - ", filename, ioe);
        }

        return new byte[0];
    }

    /**
     * Transform the given buffered image into a byte array.
     *
     * @param mergeImage Buffered image generated after merging tiff images.
     * @param filename   Filename given to the output file.
     * @return A byte array of the jpeg image. In case of failure on the process the method return null.
     */
    private byte[] getJpegImageBytes(final BufferedImage mergeImage, final String filename) {

        final File jpegFile = new File(localPath, filename);

        try {

            ImageIO.write(mergeImage, outputFormat, jpegFile);

            return readLocalFile(jpegFile.getPath());

        } catch (IllegalArgumentException | IOException ie) {

            LOG.error("Error writing result file {} - ", mergeImage, ie);

        }

        return new byte[0];
    }

    /**
     * Build the merge buffered image from the given tiff image filenames.
     *
     * @param foundImageBands A Map with key : supported band and value: list of filenames of the tiff images.
     * @return Buffered image which is the result of the tiff images merge.
     */
    private BufferedImage buildMergeImage(final Map<String, List<String>> foundImageBands) {

        BufferedImage mergeImage = null;
        Graphics2D graphics2D = null;

        for (Map.Entry<String, List<String>> foundImagesBand : foundImageBands.entrySet()) {

            for (String filename : foundImagesBand.getValue()) {

                final BufferedImage sourceImage = readGeoTiffImage(filename);

                if (null == sourceImage)
                    continue;

                if (null == mergeImage) {

                    mergeImage = new BufferedImage(
                            sourceImage.getWidth(),
                            sourceImage.getHeight(),
                            BufferedImage.TYPE_INT_RGB);

                    graphics2D = mergeImage.createGraphics();
                }

                drawImageBySupportedBand(graphics2D, sourceImage, foundImagesBand.getKey());
            }
        }

        if (null != graphics2D)
            graphics2D.dispose();

        return mergeImage;
    }

    /**
     * Draw the given source image into the graphics2D using the given found band to set the proper RGB on each pixel.
     * The mapping is defined:
     *
     * <ul>
     *  <li> BGR - Blue, Green, Red, colors supported for B02, B03, B04 sensor bands
     *  <li> RGB - Red, Green, Blue, colors supported for B05, B06, B07 sensor bands
     *  <li> B   - Blue color supported for B09 sensor band
     * </ul>
     *
     * @param graphics2D  Graphics to draw the RGB image with the respectively color associated to the band.
     * @param sourceImage Current TIFF image is going to be processed with proper RGB color masking.
     * @param foundBand   Sensor band of the current source image to draw on the given graphics2D instance.
     */
    private void drawImageBySupportedBand(Graphics2D graphics2D, final BufferedImage sourceImage,
                                          final String foundBand) {

        final double weight = 0.5d;
        final int supportedBand = getSupportedBands().getOrDefault(foundBand, 0);

        if (0 != supportedBand) {

            for (int row = 0; row < sourceImage.getWidth(); row++) {

                for (int col = 0; col < sourceImage.getHeight(); col++) {

                    final int pixel = sourceImage.getRGB(row, col) & supportedBand;

                    sourceImage.setRGB(row, col, pixel);
                }
            }

            graphics2D.drawImage(sourceImage, null, 0, 0);

            graphics2D.setComposite(
                    AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) (1.0 - weight)));
        }
    }

    /**
     * Read tiff images from the given filename.
     *
     * @param filename Filename in local storage.
     * @return Buffered image of the given filename.
     */
    private BufferedImage readGeoTiffImage(final String filename) {

        try {

            return ImageIO.read(new File(filename));

        } catch (IOException ioe) {

            LOG.error("Error reading geoTiff image file {} - ", filename, ioe);
        }

        return null;
    }

    /**
     * Retrieves a map with the key : sensor bands and value : supported RGB color.
     *
     * @return A Map instance with the sensor bands : RGB color.
     */
    private Map<String, Integer> getSupportedBands() {

        final Map<String, Integer> channelsRgb = new HashMap<>();

        channelsRgb.put("B02",
                Color.blue.getRGB());

        channelsRgb.put("B03",
                Color.green.getRGB());

        channelsRgb.put("B04",
                Color.red.getRGB());

        channelsRgb.put("B05",
                Color.red.getRGB());

        channelsRgb.put("B06",
                Color.green.getRGB());

        channelsRgb.put("B07",
                Color.blue.getRGB());

        channelsRgb.put("B09",
                Color.blue.getRGB());

        return channelsRgb;
    }

}
