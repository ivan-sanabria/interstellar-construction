# Interstellar Construction - Interview Challenge

version 2.6.0 - 09/03/2023

[![License](https://img.shields.io/badge/license-apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build](https://img.shields.io/bitbucket/pipelines/ivan-sanabria/interstellar-construction.svg)](http://bitbucket.org/ivan-sanabria/interstellar-construction/addon/pipelines/home#!/results/branch/master/page/1)
[![PR](https://img.shields.io/bitbucket/pr/ivan-sanabria/interstellar-construction.svg)](http://bitbucket.org/ivan-sanabria/interstellar-construction/pull-requests)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_interstellar-construction&metric=alert_status)](https://sonarcloud.io/project/overview?id=Iván-Camilo-Sanabria-Rincón_interstellar-construction)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_interstellar-construction&metric=bugs)](https://sonarcloud.io/project/issues?resolved=false&types=BUG&id=Iván-Camilo-Sanabria-Rincón_interstellar-construction)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_interstellar-construction&metric=coverage)](https://sonarcloud.io/component_measures?id=Iván-Camilo-Sanabria-Rincón_interstellar-construction&metric=coverage)
[![Size](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_interstellar-construction&metric=ncloc)](https://sonarcloud.io/code?id=Iván-Camilo-Sanabria-Rincón_interstellar-construction)
[![TechnicalDebt](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_interstellar-construction&metric=sqale_index)](https://sonarcloud.io/component_measures?metric=Maintainability&id=Iván-Camilo-Sanabria-Rincón_interstellar-construction)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_interstellar-construction&metric=vulnerabilities)](https://sonarcloud.io/project/issues?resolved=false&types=VULNERABILITY&id=Iván-Camilo-Sanabria-Rincón_interstellar-construction)

## Specifications

Please review the PDF file attached on the project.

## Requirements

- JDK 14.x
- Maven 3.6.3
- IDE for JAVA (Eclipse, Netbeans, IntelliJ).
- Postman 10.10.x
- JMeter 5.1.x
- [Docker 2.3.x](https://www.docker.com/products/docker-desktop)

## Application Setup

To run the application using local folder as image datasource:

1. Download the source code from repository.
2. Go to the root location of the source code.
3. Copy the tif images from your local directory to the dataset folder.

```bash
    cp -r ${LOCATION_TIFF_IMAGES}/*.tif dataset/
```

Review the values inside **src/main/resources/application.yml**

```
    dataProperties:
        localEnabled: true
        localDirectory: dataset
        outputFormat: jpeg    
    amazonProperties:
        bucketName:
```

Review the values inside **DockerFile** for local image datasource.

```
    ARG LOCAL_DATA_SET=dataset
    ADD ${LOCAL_DATA_SET} dataset
```

To run the application using S3 bucket as image datasource:

1. Set environment variables **AWS_ACCESS_KEY_ID** and **AWS_SECRET_ACCESS_KEY** in your .bash_profile to access the specified S3 bucket.
2. Download the source code from repository.
3. Go to the root location of the source code.
4. Copy the tif images from your local directory to S3 bucket.

Review the values inside **src/main/resources/application.yml**

```
    dataProperties:
        localEnabled: false
        localDirectory: dataset
        outputFormat: jpeg    
    amazonProperties:
        bucketName: ${aws-bucket-name}
```

Review the value inside **DockerFile** for S3 image datasource.

```
    ARG LOCAL_DATA_SET=dataset
    ADD ${LOCAL_DATA_SET} dataset
```

The **localDirectory** folder is set for both cases, because this folder stores the generated jpeg images and uses it 
as cache image system.

## Running Application inside IDE

To run the application on your IDE:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Integrate your project with IDE:
    - [Eclipse](http://books.sonatype.com/m2eclipse-book/reference/creating-sect-importing-projects.html)
    - [Netbeans](http://wiki.netbeans.org/MavenBestPractices)
    - [IntelliJ]( https://www.jetbrains.com/idea/help/importing-project-from-maven-model.html)
5. Open IDE and run the class **Application.java**.

## Running Application on Terminal

To run the application on terminal with Maven:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean verify
    java -jar target/interstellar-construction-2.6.0.jar
```

## Running Unit Tests on Terminal

To run the unit tests on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean test
```

## Running Application using Docker

To run the application on terminal with docker:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Verify the version of Docker - 19.x or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean package
    docker build --build-arg AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} --build-arg AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} -t interstellar-construction .
    docker run -p 8080:8080 interstellar-construction
```

## Check Application Test Coverage using Jacoco

To verify test coverage on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean verify
    open target/site/jacoco/index.html
```

## Testing using Postman Collection

To test using the postman collection:

1. Run the application inside IDE or Terminal.
2. Open Postman application.
3. Import the postman collection **interstellar-construction-collection.json**.
4. Import the postman environment **interstellar-construction-local-environment.json**.
5. Check the environment value of **image_host** point to localhost and port your application is running.
6. Move the mouse over the collection.
7. Click on the three dots on the right side.
8. Click on **Run collection**.
9. Check the number of iterations is 1.
10. Click on **Run Interstellar Construction** button.
11. Validate all tests passed.

## Load Test using JMeter

To load test the application using JMeter GUI:

1. Run the application inside IDE or Terminal.
2. Open JMeter GUI.
3. Open **interstellar-construction.jmx**.
4. Start the load test.

Be aware of the first request to specific grid, it would take longer because the images are not cached locally.

## List Dependency Licenses

To generate list of dependency licenses on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn project-info-reports:dependencies
    open target/site/dependencies.html
```

## Continuous Deployment using Bitbucket Pipelines, ECS and Fargate

### Prerequisites 

To enable bitbucket pipelines to execute continuous deployment when a PR is merge to master is required:

1. Open a terminal.
2. Install aws cli on your local system:
    - [AWS Documentation](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
3. Run aws configure command and provide aws credentials:

```bash
    aws configure
    AWS Access Key ID [None]: ${AWS_ACCESS_KEY_ID}
    AWS Secret Access Key [None]: ${AWS_SECRET_ACCESS_KEY}
    Default region name [None]: eu-central-1
    Default output format [None]: json
```

### AWS Core Infrastructure

To support continuous deployment is necessary to provide the infrastructure where is going to be deployed the new docker image version:

1. Go to the root location of the source code.
2. Execute the commands to create VPC, Subnets, Gateways, Routes, Security Groups, Roles:

```bash
   aws cloudformation create-stack --stack-name interstellar-construction-vpc --capabilities CAPABILITY_NAMED_IAM  --template-body file://./aws/aws-cloud-formation.yml
   aws cloudformation wait stack-create-complete --stack-name interstellar-construction-vpc && echo "interstellar-construction-vpc stack is created."
   aws cloudformation describe-stacks --stack-name interstellar-construction-vpc > cloud-formation-vpc-output.json 
```

### AWS ECS Infrastructure

To deploy the docker image is necessary to provide an ecs cluster, log group, task definition:

1. The cluster name should be the same as the variable **ECS_CLUSTER_NAME** at **script/bitbucket-pipelines-aws-ecs.sh**.
2. The log group name should be the same as used on **aws/aws-ecs-task-definition.json** on the **logConfiguration** section and  **script/bitbucket-pipelines-aws-ecs.sh** variable **ECS_LOG_GROUP**.
3. The log group prefix specified on the **aws/aws-ecs-task-definition.json** should be the same as variable **ECS_LOG_PREFIX** at **script/bitbucket-pipelines-aws-ecs.sh**.
4. There is some values to REPLACE on the **aws/aws-ecs-task-definition.json** that could be found at:
    - **cloud-formation-vpc-output.json**
    - **bitbucket-pipelines-aws-ecs.sh**   
5. Execute the commands to create the ecs cluster, log group and register first task definition:

```bash
    aws ecs create-cluster --cluster-name ivan-sanabria-interstellar-construction-cluster
    aws logs create-log-group --log-group-name ivan-sanabria-interstellar-construction-log-group
    aws ecs register-task-definition --cli-input-json file://./aws/aws-ecs-task-definition.json
```

### AWS Load Balancer Infrastructure

To expose the service to the internet is necessary to create a load balancer, target group and listener:

1. The public subnets values could be found at **cloud-formation-vpc-output.json**.
2. The vpc identifier could be found at  **cloud-formation-vpc-output.json**.
3. The target group ARN could be found at **cloud-formation-target-group-output.json**.
4. The load balancer ARN could be found at **cloud-formation-nlb-output.json**.
5. Execute the commands to create load balancer, target group, and lister to route traffic to the ecs cluster:

```bash
    aws elbv2 create-load-balancer --name interstellar-construction --scheme internet-facing --type network --subnets REPLACE_ME_PUBLIC_SUBNET_ONE REPLACE_ME_PUBLIC_SUBNET_TWO > cloud-formation-nlb-output.json
    aws elbv2 create-target-group --name interstellar-construction-tg --port 8080 --protocol TCP --target-type ip --vpc-id REPLACE_ME_VPC_ID --health-check-interval-seconds 30 --health-check-protocol HTTP --healthy-threshold-count 3 --unhealthy-threshold-count 3 > cloud-formation-target-group-output.json
    aws elbv2 create-listener --default-actions TargetGroupArn=REPLACE_ME_NLB_TARGET_GROUP_ARN,Type=forward --load-balancer-arn REPLACE_ME_NLB_ARN --port 80 --protocol TCP
```

### AWS ECS Service Provision

To run and register containers to the load balancer to receive traffic is necessary to provide ecs service:

1. The security group value could be found at **cloud-formation-vpc-output.json**.
2. The private subnets values could be found at **cloud-formation-vpc-output.json**.
3. The load balancer target group ARN could be found at **cloud-formation-target-group-output.json**.
4. Execute the command to create ecs service:

```bash
    aws ecs create-service --cli-input-json file://./aws/aws-ecs-service-definition.json
```

In order to validate that whole process was make properly execute the following commands:

```bash
    curl -X GET http://${NLB_DNS_NAME}/status
    curl -X GET http://${NLB_DNS_NAME}/info
    curl -X POST -H "Content-Type:application/json" -d '{"utmZone":33,"latitudeBand":"U","gridSquare":"UP","date":"2018-08-20","channelMap":"visible"}' http://${NLB_DNS_NAME}/generate-image > response.jpeg
```

To save cost on AWS it is recommended to terminate the environment after the exercise is covered.

### Bitbucket Pipelines Setup

To finish continuous deployment, setup the repository variables:

1. Open a browser.
2. Go to bitbucket repository.
3. Click on interstellar-construction repository > Settings > Repository Variables.
4. Add the variables **ECS_SERVICE_ROLE_ARN** and **ECS_TASK_ROLE_ARN** and the values could be found at **cloud-formation-vpc-output.json**.

## API Documentation

To see swagger documentation:

1. Open a browser
2. Go to http://${NLB_DNS_NAME}/swagger-ui.html

# Contact Information

Email: icsanabriar@googlemail.com