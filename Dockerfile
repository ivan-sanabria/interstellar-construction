FROM adoptopenjdk/openjdk14:ubi

LABEL maintainer="icsanabriar@googlemail.com"

ARG AWS_ACCESS_KEY_ID=""
ENV AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID

ARG AWS_SECRET_ACCESS_KEY=""
ENV AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY

ARG LOCAL_DATA_SET=dataset
ADD ${LOCAL_DATA_SET} dataset

ARG JAR_FILE=target/interstellar-construction-*.jar
ADD ${JAR_FILE} interstellar-construction.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/interstellar-construction.jar"]

EXPOSE 8080